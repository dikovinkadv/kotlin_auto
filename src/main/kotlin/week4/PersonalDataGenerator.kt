package week4

import com.google.gson.GsonBuilder
import com.itextpdf.io.font.PdfEncodings
import com.itextpdf.kernel.font.PdfFontFactory
import com.itextpdf.kernel.geom.PageSize
import com.itextpdf.kernel.pdf.PdfDocument
import com.itextpdf.kernel.pdf.PdfWriter
import com.itextpdf.layout.Document
import com.itextpdf.layout.element.Cell
import com.itextpdf.layout.element.Paragraph
import com.itextpdf.layout.element.Table
import java.io.File
import java.nio.file.Paths
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import kotlin.random.Random

const val MIN_PEOPLE = 1
const val MAX_PEOPLE = 30
const val MIN_AGE = 18
const val MAX_AGE = 85
const val MIN_POSTAL_CODE = 100000
const val MAX_POSTAL_CODE = 999999
const val MIN_HOUSE_NUMBER = 1
const val MAX_HOUSE_NUMBER = 100
const val MIN_APARTMENT_NUMBER = 1
const val MAX_APARTMENT_NUMBER = 500
const val FONT_PATH = "src/main/resources/Arial Unicode.ttf"

data class Person(
    val firstName: String,
    val lastName: String,
    val patronymic: String,
    val age: Int,
    val gender: String,
    val dateOfBirth: String,
    val placeOfBirth: String,
    val address: Address
)

data class Address(
    val postalCode: String,
    val country: String,
    val region: String,
    val city: String,
    val street: String,
    val house: String,
    val apartment: String
)

fun generateRandomPerson(): Person {
    val genders = listOf("м", "ж")
    val maleNames = listOf("Иван", "Александр", "Михаил", "Сергей", "Андрей")
    val femaleNames = listOf("Екатерина", "Ольга", "Татьяна", "Анна", "Мария")
    val maleLastNames = listOf("Иванов", "Петров", "Смирнов", "Сидоров", "Кузнецов")
    val femaleLastNames = listOf("Иванова", "Петрова", "Смирнова", "Сидорова", "Кузнецова")
    val malePatronymics = listOf("Иванович", "Александрович", "Михайлович", "Сергеевич", "Андреевич")
    val femalePatronymics = listOf("Ивановна", "Александровна", "Михайловна", "Сергеевна", "Андреевна")
    val countries = listOf("Россия", "Украина", "Беларусь", "Казахстан", "Армения")
    val regions = listOf("Московская область", "Ленинградская область", "Свердловская область", "Ростовская область", "Татарстан")
    val cities = listOf("Москва", "Санкт-Петербург", "Екатеринбург", "Ростов-на-Дону", "Казань")
    val streets = listOf("Ленина", "Пушкина", "Гагарина", "Советская", "Мира")

    val gender = genders.random()
    val firstName = if (gender == "м") maleNames.random() else femaleNames.random()
    val lastName = if (gender == "м") maleLastNames.random() else femaleLastNames.random()
    val patronymic = if (gender == "м") malePatronymics.random() else femalePatronymics.random()
    val age = Random.nextInt(MIN_AGE, MAX_AGE)
    val dateOfBirth = LocalDate.now().minusYears(age.toLong()).format(DateTimeFormatter.ofPattern("dd-MM-yyyy"))
    val placeOfBirth = cities.random()
    val postalCode = Random.nextInt(MIN_POSTAL_CODE, MAX_POSTAL_CODE).toString()
    val country = countries.random()
    val region = regions.random()
    val city = cities.random()
    val street = streets.random()
    val house = Random.nextInt(MIN_HOUSE_NUMBER, MAX_HOUSE_NUMBER).toString()
    val apartment = Random.nextInt(MIN_APARTMENT_NUMBER, MAX_APARTMENT_NUMBER).toString()

    return Person(firstName, lastName, patronymic, age, gender, dateOfBirth, placeOfBirth,
        Address(postalCode, country, region, city, street, house, apartment))
}

fun generateRandomPersons(count: Int): List<Person> {
    return List(count) { generateRandomPerson() }
}

fun createJsonFile(persons: List<Person>, fileName: String) {
    val gson = GsonBuilder().setPrettyPrinting().create()
    File(fileName).writeText(gson.toJson(persons), Charsets.UTF_8)
    val absolutePath = Paths.get(fileName).toAbsolutePath().toString()
    println("Файл JSON создан. Путь: $absolutePath")
}

fun createPdfTable(persons: List<Person>): Table {
    val table = Table(14)
    val pdfFont = PdfFontFactory.createFont(FONT_PATH, PdfEncodings.IDENTITY_H, PdfFontFactory.EmbeddingStrategy.PREFER_EMBEDDED)

    table.setFontSize(7f)
    table.setFont(pdfFont)

    val headers = arrayOf(
        "Имя", "Фамилия", "Отчество", "Возраст", "Пол", "Дата рождения", "Место рождения",
        "Индекс", "Страна", "Область", "Город", "Улица", "Дом", "Квартира")

    headers.forEach { header ->
        table.addCell(Cell().add(Paragraph(header)))
    }

    persons.forEach { person ->
        val values = listOf(
            person.firstName, person.lastName, person.patronymic, person.age.toString(), person.gender,
            person.dateOfBirth, person.placeOfBirth, person.address.postalCode, person.address.country,
            person.address.region, person.address.city, person.address.street, person.address.house, person.address.apartment)

        for (value in values) {
            table.addCell(Cell().add(Paragraph(value)))
        }
    }

    return table
}

fun createPdfFile(persons: List<Person>, fileName: String) {
    val writer = PdfWriter(fileName)
    val pdfDocument = PdfDocument(writer)
    pdfDocument.defaultPageSize = PageSize.A4.rotate()

    val document = Document(pdfDocument)
    document.setMargins(20f, 20f, 20f, 20f)

    val table = createPdfTable(persons)
    document.add(table)

    document.close()

    println("Файл PDF создан. Путь: ${File(fileName).absoluteFile}")
}

fun main() {
    validateAndGeneratePersons()
}

fun validateAndGeneratePersons() {
    println("Введите количество людей (от $MIN_PEOPLE до $MAX_PEOPLE):")
    val count = readlnOrNull()?.toIntOrNull()

    if (count == null || count !in MIN_PEOPLE..MAX_PEOPLE) {
        println("Некорректный ввод. Программа будет завершена.")
        return
    }

    val persons = generateRandomPersons(count)
    val jsonFileName = "persons.json"
    val pdfFileName = "persons.pdf"

    createJsonFile(persons, jsonFileName)
    createPdfFile(persons, pdfFileName)
}
