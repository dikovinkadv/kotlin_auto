package week5

import week2.calculator
import week2.getCalculationMethod

class Calculation {

    companion object {
        const val SUM = "+"
        const val SUBTRACTION = "-"
        const val MULTIPLICATION = "*"
        const val DIVISION = "/"
        const val INVALID_OPERATION = "#"
    }

    val sum = { a: Double, b: Double -> a + b }
    val subtraction = { a: Double, b: Double -> a - b }
    val multiplication = { a: Double, b: Double -> a * b }
    val division = { a: Double, b: Double -> a / b }

    fun calculator(lambda : ((Double, Double)-> Double), a: Double, b: Double ): Double {
        return lambda(a,b)
    }

    fun getCalculationMethod(name: String) :(Double, Double)-> Double {
        return when (name) {
            SUM -> sum
            SUBTRACTION -> subtraction
            MULTIPLICATION -> multiplication
            DIVISION -> division
            else -> throw UnsupportedOperationException()
        }
    }
}

fun main() {
    println(calculator(getCalculationMethod(Calculation.SUM), 2.0, 3.0))
    println(calculator(getCalculationMethod(Calculation.SUBTRACTION), 2.0, 13.0))
    println(calculator(getCalculationMethod(Calculation.MULTIPLICATION), 5.0, 5.5))
    println(calculator(getCalculationMethod(Calculation.DIVISION), 20.0, 2.0))
    println(calculator(getCalculationMethod(Calculation.DIVISION), 20.0, 0.0))
}
