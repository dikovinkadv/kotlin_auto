package week2

abstract class Pet

interface Runnable {
    fun run()
}

interface Swimmable {
    fun swim()
}

open class Cat : Pet(), Runnable, Swimmable {
    override fun run() {
        println("I am a Cat, and I am running")
    }

    override fun swim() {
        println("I am a Cat, and I am swimming")
    }
}

open class Fish : Pet(), Swimmable {
    override fun swim() {
        println("I am a Fish, and I am swimming")
    }
}

class Tiger : Cat(), Runnable, Swimmable {
    override fun run() {
        println("I'm Tiger, and i running")
    }

    override fun swim() {
        println("I'm Tiger, and i swimming")
    }
}

class Lion : Cat(), Runnable, Swimmable {
    override fun run() {
        println("I'm Lion, and i running")
    }

    override fun swim() {
        println("I'm Lion, and i swimming")
    }
}

class Salmon : Fish(), Swimmable {
    override fun swim() {
        println("I'm Salmon, and i swimming")
    }
}

class Tuna : Fish(), Swimmable {
    override fun swim() {
        println("I'm Tuna, and i swimming")
    }
}

fun <T : Runnable> useRunSkill(pet: T) {
    pet.run()
}

fun <T : Swimmable> useSwimSkill(pet: T) {
    pet.swim()
}

fun <T> useSwimAndRunSkill(pet: T) where T:Runnable, T:Swimmable {
    pet.run()
    pet.swim()
}

fun main() {
    val shram = Lion()
    useRunSkill(shram)
    useSwimSkill(shram)
    useSwimAndRunSkill(shram)

    val sherhan = Tiger()
    useRunSkill(sherhan)
    useSwimSkill(sherhan)
    useSwimAndRunSkill(sherhan)

    val lososik = Salmon()
    useSwimSkill(lososik)

    val tunco = Tuna()
    useSwimSkill(tunco)
}
