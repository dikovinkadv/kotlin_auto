package week2

fun operationsWithCollections(numbers: List<Double?>): Double {
        return numbers
            .filterNotNull()
            .map { if (it.toInt() % 2 == 0) it * it else it / 2}
            .filter { it <= 25.0 }
            .sortedDescending()
            .take(10)
            .sum()
}

fun main() {
    val collection1 = listOf(13.31, 3.98, 12.0, 2.99, 9.0)
    val result1 = operationsWithCollections(collection1)
    println("%.2f".format(result1)) // Вывод с округлением до двух знаков после запятой

    val collection2 = listOf(133.21, null, 233.98, null, 26.99, 5.0, 7.0, 9.0)
    val result2 = operationsWithCollections(collection2)
    println("%.2f".format(result2)) // Вывод с округлением до двух знаков после запятой
}
