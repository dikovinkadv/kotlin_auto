package week2

val coefficients1 = intArrayOf(7, 2, 4, 10, 3, 5, 9, 4, 6, 8)
val coefficients2 = intArrayOf(3, 7, 2, 4, 10, 3, 5, 9, 4, 6, 8)

val validateInn: (String) -> Boolean = { inn ->
    // Проверка на длину ИНН и наличие только цифр
    if (inn.length != 12 || !inn.all { it.isDigit() }) {
        false
    } else {
        val checksum1 = calculateSum(inn, coefficients1)
        val checksum2 = calculateSum(inn, coefficients2)

        val controlDigit1 = Character.getNumericValue(inn[10])
        val controlDigit2 = Character.getNumericValue(inn[11])

        // Вычисление младшего разряда остатка от деления
        val remainder1 = (checksum1 % 11) % 10
        val remainder2 = (checksum2 % 11) % 10

        // Проверка контрольных цифр ИНН
        remainder1 == controlDigit1 && remainder2 == controlDigit2
    }
}

fun calculateSum(inn: String, coefficients: IntArray): Int {
    var sum = 0
    for (i in coefficients.indices) {
        sum += coefficients[i] * Character.getNumericValue(inn[i])
    }
    return sum
}

fun validateInnFunction(inn: String, function: (String) -> Boolean) {
    println("ИНН $inn ${if (function(inn)) "валиден" else "не валиден"}")
}

fun main() {
    val inn = "707574944998"
    validateInnFunction(inn, validateInn)
}
