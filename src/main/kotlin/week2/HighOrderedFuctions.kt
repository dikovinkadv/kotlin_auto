package week2

val sum = { a: Double, b: Double -> a + b }
val subtraction = { a: Double, b: Double -> a - b }
val multiplication = { a: Double, b: Double -> a * b }
val division = { a: Double, b: Double -> a / b }

fun main() {
    calculator(getCalculationMethod("+"), 2.0, 3.0)
    calculator(getCalculationMethod("-"), 2.0, 13.0)
    calculator(getCalculationMethod("*"), 5.0, 5.5)
    calculator(getCalculationMethod("/"), 20.0, 2.0)

}

fun calculator(lambda : ((Double, Double)-> Double), a: Double, b: Double ) {
    println(lambda(a,b))
}

fun getCalculationMethod(name: String) :(Double, Double)-> Double {
    return when (name) {
        "+" -> sum
        "-" -> subtraction
        "*" -> multiplication
        "/" -> division
        else -> throw UnsupportedOperationException()
    }
}
