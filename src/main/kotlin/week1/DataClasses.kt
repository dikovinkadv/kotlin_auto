package week1

data class Person(
    val name: String,
    val surName: String,
    val patronymic: String,
    val gender: String,
    val dateOfBirth: String,
    val age: Int,
    val inn: String?,
    val snils: String?
)

fun createPerson(): Person {
    val name = "Руслан"
    val surName = "Плиев"
    val patronymic = "Эмзарович"
    val gender = "Мужской"
    val dateOfBirth = "1999-10-21"
    val age = 24
    val inn: String? = null
    val snils: String? = null

    return Person(name, surName, patronymic, gender, dateOfBirth, age, inn, snils)
}

fun main() {
    val person = createPerson()
    println(person)
}
