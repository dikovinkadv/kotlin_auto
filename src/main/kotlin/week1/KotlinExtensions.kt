package week1

fun MutableList<Int>.squareAllElements() {
    for (i in indices) {
        this[i] = this[i] * this[i]
    }
}

fun main() {
    val numbers = mutableListOf(1, 4, 9, 16, 25)
    println("Исходная коллекция: $numbers")
    numbers.squareAllElements()
    println("Коллекция после возведения в квадрат: $numbers")
}
