package week1

class MyClass {
    companion object {
        private var counter = 0

        fun counter() {
            counter++
            println("Вызван counter. Количество вызовов = $counter")
        }
    }
}

fun main() {
    MyClass.counter()
    MyClass.counter()
}
