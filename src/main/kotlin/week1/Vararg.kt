package week1

fun printVarargs(vararg strings: String) {
    val count = strings.size
    val joinedArgs = strings.joinToString(";")

    println("Передано $count элемента: \n$joinedArgs")
}

fun main() {
    printVarargs("12", "122", "1234", "fpo")
}
