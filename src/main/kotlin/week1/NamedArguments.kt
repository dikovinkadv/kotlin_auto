package week1

fun configurePersonData(
    firstName: String,
    lastName: String,
    middleName: String,
    gender: String,
    dateOfBirth: String,
    age: Int,
    inn: String? = null,
    snils: String? = null
) {
    val personInfo = """
        |Имя: $firstName
        |Фамилия: $lastName
        |Отчество: $middleName
        |Пол: $gender
        |Дата рождения: $dateOfBirth
        |Возраст: $age
    """.trimMargin()

    println(personInfo)
    inn?.let { println("ИНН: $it") }
    snils?.let { println("СНИЛС: $it") }
}

fun main() {
    println("Вызов с обязательными параметрами:")
    configurePersonData(
        firstName = "Иван",
        lastName = "Иванов",
        middleName = "Иванович",
        gender = "Мужской",
        dateOfBirth = "01.01.2004",
        age = 20
    )

    println("\nВызов с необязательными параметрами:")
    configurePersonData(
        firstName = "Петр",
        lastName = "Петров",
        middleName = "Петрович",
        gender = "Мужской",
        dateOfBirth = "01.01.1990",
        age = 34,
        inn = "123456789012",
        snils = "123-456-789 01"
    )

    println("\nВызов в произвольном порядке параметров:")
    configurePersonData(
        lastName = "Сидоров",
        dateOfBirth = "02.02.2000",
        firstName = "Алексей",
        age = 24,
        gender = "Мужской",
        middleName = "Михайлович",
        inn = "123464353012"
    )
}
