package week1

import java.time.LocalDate
import java.time.Month
import java.util.logging.Handler

fun typeCasting(obj: Any?) {
    val typeName = obj?.javaClass?.simpleName ?: "null"
    when (obj) {
        is String -> {
            val length = obj.length
            println("Я получил $typeName = '$obj', ее длина равна $length")
        }
        is Int -> {
            val square = obj * obj
            println("Я получил $typeName = $obj, его квадрат равен $square")
        }
        is Double -> {
            val roundedValue = if (obj.toLong().toDouble() == obj) {
                obj.toLong()
            } else {
                "%.2f".format(obj)
            }
            println("Я получил $typeName = $obj, это число округляется до $roundedValue")
        }
        is LocalDate -> {
            val tinkoffFoundationDate = LocalDate.of(2006, Month.DECEMBER, 24)
            val comparisonResult = if (obj.isBefore(tinkoffFoundationDate)) {
                "меньше"
            } else {
                "больше"
            }
            println("Я получил $typeName $obj, она $comparisonResult даты основания Tinkoff")
        }
        null -> {
            println("Объект равен null")
        }
        else -> {
            println("Мне этот тип неизвестен")
        }
    }
}

fun main() {
    typeCasting("Privet")
    typeCasting(145)
    typeCasting(145.0)
    typeCasting(145.2817812)
    typeCasting(LocalDate.of(1990, 1, 1))
    typeCasting(Handler::class)
}
