package week5

import org.junit.Assert.*
import org.junit.Test

class CalculationNegativeTest {
    private val calculation = Calculation()

    @Test
    fun invalidOperationTest() {
        assertThrows(UnsupportedOperationException::class.java) {
            calculation.calculator(
                calculation.getCalculationMethod(Calculation.Companion.INVALID_OPERATION),
                20.0,
                2.0
            )
        }
    }

    @Test
    fun largeNumbersTest() {
        val result = calculation.calculator(
            calculation.getCalculationMethod(Calculation.SUBTRACTION),
            1e10,
            2e10
        )
        assertEquals(-1e10, result, 0.001)
    }

    @Test
    fun divisionByZeroTest() {
        assertThrows(ArithmeticException::class.java) {
            calculation.calculator(
                calculation.getCalculationMethod(Calculation.DIVISION),
                20.0,
                0.0
            )
        }
    }
}
