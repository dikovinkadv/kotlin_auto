package week5

import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized

@RunWith(Parameterized::class)
class CalculationPositiveTest(
    private val operation: String,
    private val firstNumber: Double,
    private val secondNumber: Double,
    private val expected: Double
) {
    private val calculation = Calculation()

    companion object {
        @JvmStatic
        @Parameterized.Parameters
        fun data(): List<Array<Any>> {
            return listOf(
                arrayOf(Calculation.SUM, 2.0, -3.0, -1.0),
                arrayOf(Calculation.SUBTRACTION, 2.0, 13.0, -11.0),
                arrayOf(Calculation.MULTIPLICATION, 5.0, 5.5, 27.5),
                arrayOf(Calculation.DIVISION, 20.0, 2.0, 10.0)
            )
        }
    }

        @Test
        fun calculationMethodTest() {
            val result = calculation.calculator(
                calculation.getCalculationMethod(operation),
                firstNumber,
                secondNumber
            )
            assertEquals(expected, result, 0.001)
        }
    }
